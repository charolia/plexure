﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exercise2.CouponManagement.Models
{
    [Table("CouponUserCount")]
    public class CouponUserCount
    {
        //[Key, Column(Order =0)]
        [ForeignKey("FK_CouponRedemptionHistory_User")]
        public int UserId { get; set; }

        //[Key, Column(Order = 1)]
        [ForeignKey("FK_CouponRedemptionHistory_Coupon")]
        public long CouponId { get; set; }

        public int TotalRedeem { get; set; }
    }
}
