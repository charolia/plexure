﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exercise2.CouponManagement.Models
{
    [Table("CouponRedemptionHistory")]
    public class CouponRedemptionHistory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public long Id { get; set; }
        
        [ForeignKey("FK_CouponRedemptionHistory_User")]
        public int UserId { get; set; }

        [ForeignKey("FK_CouponRedemptionHistory_Coupon")]
        public long CouponId { get; set; }
        public DateTime Date { get; set; }
        public Guid Code { get; set; }
    }
}
