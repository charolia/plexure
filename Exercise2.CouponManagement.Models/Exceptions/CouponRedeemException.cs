﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Exercise2.CouponManagement.Models.Exceptions
{
    public class CouponRedeemException : System.Exception
    {
        public CouponRedeemException() : base()
        { 
        
        }

        public CouponRedeemException(string? message) : base(message)
        { 
        }

        public CouponRedeemException(string? message, Exception? innerException) : base(message, innerException)
        { 
        
        }

        protected CouponRedeemException(SerializationInfo info, StreamingContext context) : base(info, context)
        { 

        }
    }
}
