﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Exercise2.CouponManagement.Models
{
    [Table("CouponAllUserCount")]
    public class CouponAllUserCount
    {
        [Key]
        public long CouponId { get; set; }
        public int TotalRedeem { get; set; }
    }
}
