﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exercise2.CouponManagement.Models.Reports.Models
{
    public class CouponRedemptionReportModel
    {
        public Coupon Coupon { get; set; }

        public List<CouponRedemptionHistory> UserRedemptionHistory { get; set; }

        public int CouponClaimedByAllTheUsers { get; set; }
    }
}
