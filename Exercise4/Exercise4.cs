
/// <summary>
/// Provides capabilities for managing a customers itinerary.
/// </summary>

/*
-----------------------------------------------------------------------------------------------------------
*************************************         CODE COMMENTS           *************************************
-----------------------------------------------------------------------------------------------------------
1. ItineraryManager should implement an interface named 'IItineraryManager'
-----------------------------------------------------------------------------------------------------------
*/
public class ItineraryManager
{
    private readonly IDataStore _dataStore;
    private readonly IDistanceCalculator _distanceCalculator;
 
    public ItineraryManager()
    {
        /*
        -----------------------------------------------------------------------------------------------------------
        *************************************         CODE COMMENTS           *************************************
        -----------------------------------------------------------------------------------------------------------
        1.  _dataStore and _distanceCalculator shoud be injected through constructor as parameters and should not 
            be intialized here.
        2. _dataStore and _distanceCalculator after receiving as parameters should be checked for null and log or 
            through null reference exception.
        3. ConfigurationManager should not be referred here.
        4. Logger is missing. A reference to a logger implementation should be injected through constructor, and 
            should be checked for null and log or through null reference exception.
        5. Constructor is missing comments.
        
        -----------------------------------------------------------------------------------------------------------
        */

        _dataStore = new SqlAgentStore(ConfigurationManager.ConnectionStrings["SqlDbConnection"].ConnectionString);
        _distanceCalculator = new GoogleMapsDistanceCalculator(ConfigurationManager.AppSettings["GoogleMapsApiKey"]);
    }
 
    /// <summary>
    /// Calculates a quote for a customers itinerary from a provided list of airline providers.
    /// </summary>
    /// <param name="itineraryId">The identifier of the itinerary</param>
    /// <param name="priceProviders">A collection of airline price providers.</param>
    /// <returns>A collection of quotes from the different airlines.</returns>
    public IEnumerable<Quote> CalculateAirlinePrices(int itineraryId, IEnumerable<IAirlinePriceProvider> priceProviders)
    {
        var itinerary = _dataStore.GetItinaryAsync(itineraryId).Result;
        if (itinerary == null)
            throw new InvalidOperationException();
 
        List<Quote> results = new List<Quote>();
        Parallel.ForEach(priceProviders, provider =>
        {
            var quotes = provider.GetQuotes(itinerary.TicketClass, itinerary.Waypoints);
            foreach (var quote in quotes)
                results.Add(quote);
        });
        return results;

        /*
        -----------------------------------------------------------------------------------------------------------
        *************************************         CODE COMMENTS           *************************************
        -----------------------------------------------------------------------------------------------------------
        1. If itineraryId is not supoosed to be negative then it should be validated and error must be logged and 
            and argument invalid exceptions should be thrown.     
        2. Same goes for priceProviders shoule validated, log an error and throw an exception.
        3. This function is missing 'async' keyword and return type should be Task<IEnumerable<Quote>>
        4. The function name should be renamed to CalculateAirlinePricesAsync
        5. Line 48. No need to call .Result and await keyword should be used. eg. await _dataStore.GetItinaryAsync(itineraryId);
        6. Line 50. I don't think that InvalidOperationException is correct exception to through. It can be a
            KeyNotFoundException. 
        7. A Cancellation Token should passed as an function parameter and provide as an input the to Parallel.ForEach.
        8. Line 55. Result of the GetQuotes should be checked for null value and log error/warning if required.
        9. Line 56. results.AddRange(quotes) should be used instead of adding individual quote in the results list.
        10. Line 57. List is not thread safe. Either we use a lock here or we use concurrent collection type.
        11. Line 55. provider.GetQuotes can be refactored to async method.
        -----------------------------------------------------------------------------------------------------------
        */
    }
 
    /// <summary>
    /// Calculates the total distance traveled across all waypoints in a customers itinerary.
    /// </summary>
    /// <param name="itineraryId">The identifier of the itinerary</param>
    /// <returns>The total distance traveled.</returns>
    public async Task<double> CalculateTotalTravelDistanceAsync(int itineraryId)
    {
        var itinerary = await _dataStore.GetItinaryAsync(itineraryId);
        if (itinerary == null)
            throw new InvalidOperationException();
        double result = 0;
        for(int i=0; i<itinerary.Waypoints.Count-1; i++)
        {
            result = result + _distanceCalculator.GetDistanceAsync(itinerary.Waypoints[i], itinerary.Waypoints[i + 1]).Result;
        }
        return result;

        /*
        -----------------------------------------------------------------------------------------------------------
        *************************************         CODE COMMENTS           *************************************
        -----------------------------------------------------------------------------------------------------------
        1. If itineraryId is not supoosed to be negative then it should be validated and error must be logged and 
            and argument invalid exceptions should be thrown.     
        2. Line 90. I don't think that InvalidOperationException is correct exception to through. It can be a 
            KeyNotFoundException. 
        3. Line 92. Parallel.ForEach is a better choice and it will improve the performance.
        4. A Cancellation Token should passed as an function parameter and provide as an input the to Parallel.ForEach.
        5. Result of the GetQuotes should be checked for null value and log error/warning if required.
        6. Line 94. double is not thread safe and it will no produce the correct result. We can make use of 
            Interlocked.Add method but it takes int32 or int64/long and change the result type to int or long. Other 
            solution is that we introduce locking arround addition part.
        7. Line  94. await keyword missing before GetDistanceAsync function call and no need or .Result call.
        -----------------------------------------------------------------------------------------------------------
        */
    }

    /// <summary>
    /// Loads a Travel agents details from Storage
    /// </summary>
    /// <param name="id">The id of the travel agent.</param>
    /// <param name="updatedPhoneNumber">If set updates the agents phone number.</param>
    /// <returns>The travel agent if located, otherwise null.</returns>
    public TravelAgent FindAgent(int id, string updatedPhoneNumber)
    {
        var agentDao = _dataStore.GetAgent(id);
        if (agentDao == null)
            return null;
        if (!string.IsNullOrWhiteSpace(updatedPhoneNumber))
        {
            agentDao.PhoneNumber = updatedPhoneNumber;
            _dataStore.UpdateAgent(id, agentDao);
        }
        return Mapper.Map<TravelAgent>(agentDao);

        /*
        -----------------------------------------------------------------------------------------------------------
        *************************************         CODE COMMENTS           *************************************
        -----------------------------------------------------------------------------------------------------------
        1. FindAgent function should not be responsible for updating the phone number. There should be a separate 
            function to update the phone number. 
        2. If id variable is not supoosed to be negative then it should be validated and error must be logged and 
            and argument invalid exceptions should be thrown.     
        3. Line 129. updatedPhoneNumber should also be validated for a valid phone number using regex library. 
            Incase of invalid phone number, log an error/warning or throw an exception.
        4. We can convert this method and _dataStore functions to async.
        -----------------------------------------------------------------------------------------------------------
        */
    }
}