using Exercise2.CouponManagement.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise2.CouponManagement.Client
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var services = new ServiceCollection();

            var configuration = new ConfigurationBuilder()
              //.SetBasePath(System.IO.Packaging.Package.Current.InstalledLocation.Path)
              .AddJsonFile("appsettings.json", optional: false)
              .Build();


            ConfigureServices(services, configuration );

            services.AddLogging(conf => conf.AddSerilog());

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using (var serviceProvider = services.BuildServiceProvider())
            {
                Application.Run(new Form1(()=> serviceProvider.GetService<ICouponManager>()));
            }
        }

        private static void ConfigureServices(ServiceCollection services, IConfiguration configuration)
        {
            Exercise2.CouponManagement.Services.BootStrap.SetupDependencies(services, () => configuration["CouponConnectionString"]);
        }
    }
}
