﻿namespace Exercise2.CouponManagement.Client
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRedeemCoupon = new System.Windows.Forms.Button();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.txtCouponId = new System.Windows.Forms.TextBox();
            this.btnCanRedeem = new System.Windows.Forms.Button();
            this.txtCouponRedeemStatus = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lvActiveCoupons = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.btnShowHistory = new System.Windows.Forms.Button();
            this.cbCouponOffers = new System.Windows.Forms.ComboBox();
            this.lvCouponHistory = new System.Windows.Forms.ListView();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTotalCouponCount = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnRedeemCoupon
            // 
            this.btnRedeemCoupon.Location = new System.Drawing.Point(347, 110);
            this.btnRedeemCoupon.Name = "btnRedeemCoupon";
            this.btnRedeemCoupon.Size = new System.Drawing.Size(75, 23);
            this.btnRedeemCoupon.TabIndex = 2;
            this.btnRedeemCoupon.Text = "Redeem";
            this.btnRedeemCoupon.UseVisualStyleBackColor = true;
            this.btnRedeemCoupon.Click += new System.EventHandler(this.btnRedeemCoupon_Click);
            // 
            // txtUserId
            // 
            this.txtUserId.Location = new System.Drawing.Point(28, 57);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(225, 23);
            this.txtUserId.TabIndex = 3;
            // 
            // txtCouponId
            // 
            this.txtCouponId.Location = new System.Drawing.Point(306, 57);
            this.txtCouponId.Name = "txtCouponId";
            this.txtCouponId.Size = new System.Drawing.Size(226, 23);
            this.txtCouponId.TabIndex = 4;
            // 
            // btnCanRedeem
            // 
            this.btnCanRedeem.Location = new System.Drawing.Point(428, 110);
            this.btnCanRedeem.Name = "btnCanRedeem";
            this.btnCanRedeem.Size = new System.Drawing.Size(104, 23);
            this.btnCanRedeem.TabIndex = 2;
            this.btnCanRedeem.Text = "Can Redeem";
            this.btnCanRedeem.UseVisualStyleBackColor = true;
            this.btnCanRedeem.Click += new System.EventHandler(this.btnCanRedeem_Click);
            // 
            // txtCouponRedeemStatus
            // 
            this.txtCouponRedeemStatus.Location = new System.Drawing.Point(28, 198);
            this.txtCouponRedeemStatus.Multiline = true;
            this.txtCouponRedeemStatus.Name = "txtCouponRedeemStatus";
            this.txtCouponRedeemStatus.ReadOnly = true;
            this.txtCouponRedeemStatus.Size = new System.Drawing.Size(504, 290);
            this.txtCouponRedeemStatus.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "User ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(303, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "Coupon ID";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 43);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Active Coupon List";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Message";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 26);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 2;
            this.tabControl1.Size = new System.Drawing.Size(560, 537);
            this.tabControl1.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lvActiveCoupons);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(552, 509);
            this.tabPage1.TabIndex = 1;
            this.tabPage1.Text = "Active Coupons";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtTotalCouponCount);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.lvCouponHistory);
            this.tabPage2.Controls.Add(this.cbCouponOffers);
            this.tabPage2.Controls.Add(this.btnShowHistory);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(552, 509);
            this.tabPage2.TabIndex = 3;
            this.tabPage2.Text = "Report";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(374, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(166, 31);
            this.button1.TabIndex = 0;
            this.button1.Text = "Get Active Coupons";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnCanRedeem);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.txtUserId);
            this.tabPage3.Controls.Add(this.txtCouponRedeemStatus);
            this.tabPage3.Controls.Add(this.label1);
            this.tabPage3.Controls.Add(this.txtCouponId);
            this.tabPage3.Controls.Add(this.btnRedeemCoupon);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(552, 509);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Redeem Coupon";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lvActiveCoupons
            // 
            this.lvActiveCoupons.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvActiveCoupons.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.lvActiveCoupons.FullRowSelect = true;
            this.lvActiveCoupons.GridLines = true;
            this.lvActiveCoupons.HideSelection = false;
            this.lvActiveCoupons.Location = new System.Drawing.Point(14, 72);
            this.lvActiveCoupons.Name = "lvActiveCoupons";
            this.lvActiveCoupons.Size = new System.Drawing.Size(526, 419);
            this.lvActiveCoupons.TabIndex = 7;
            this.lvActiveCoupons.UseCompatibleStateImageBehavior = false;
            this.lvActiveCoupons.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Coupon ID";
            this.columnHeader1.Width = 75;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Title";
            this.columnHeader2.Width = 120;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Per User Limit";
            this.columnHeader3.Width = 85;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Total Coupons";
            this.columnHeader4.Width = 90;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Start Date";
            this.columnHeader5.Width = 75;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "End Date";
            this.columnHeader6.Width = 75;
            // 
            // btnShowHistory
            // 
            this.btnShowHistory.Location = new System.Drawing.Point(416, 25);
            this.btnShowHistory.Name = "btnShowHistory";
            this.btnShowHistory.Size = new System.Drawing.Size(119, 23);
            this.btnShowHistory.TabIndex = 0;
            this.btnShowHistory.Text = "Show History";
            this.btnShowHistory.UseVisualStyleBackColor = true;
            this.btnShowHistory.Click += new System.EventHandler(this.btnShowHistory_Click);
            // 
            // cbCouponOffers
            // 
            this.cbCouponOffers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCouponOffers.FormattingEnabled = true;
            this.cbCouponOffers.Location = new System.Drawing.Point(14, 25);
            this.cbCouponOffers.Name = "cbCouponOffers";
            this.cbCouponOffers.Size = new System.Drawing.Size(396, 23);
            this.cbCouponOffers.TabIndex = 1;
            // 
            // lvCouponHistory
            // 
            this.lvCouponHistory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lvCouponHistory.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader10,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9});
            this.lvCouponHistory.FullRowSelect = true;
            this.lvCouponHistory.GridLines = true;
            this.lvCouponHistory.HideSelection = false;
            this.lvCouponHistory.Location = new System.Drawing.Point(14, 124);
            this.lvCouponHistory.Name = "lvCouponHistory";
            this.lvCouponHistory.Size = new System.Drawing.Size(521, 370);
            this.lvCouponHistory.TabIndex = 2;
            this.lvCouponHistory.UseCompatibleStateImageBehavior = false;
            this.lvCouponHistory.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Code";
            this.columnHeader7.Width = 200;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Redeem Date";
            this.columnHeader8.Width = 95;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "User Id";
            this.columnHeader9.Width = 95;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Coupon Id";
            this.columnHeader10.Width = 85;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "Total Coupons";
            // 
            // txtTotalCouponCount
            // 
            this.txtTotalCouponCount.Location = new System.Drawing.Point(101, 77);
            this.txtTotalCouponCount.Name = "txtTotalCouponCount";
            this.txtTotalCouponCount.ReadOnly = true;
            this.txtTotalCouponCount.Size = new System.Drawing.Size(126, 23);
            this.txtTotalCouponCount.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 575);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnRedeemCoupon;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.TextBox txtCouponId;
        private System.Windows.Forms.Button btnCanRedeem;
        private System.Windows.Forms.TextBox txtCouponRedeemStatus;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ListView lvActiveCoupons;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtTotalCouponCount;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListView lvCouponHistory;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ComboBox cbCouponOffers;
        private System.Windows.Forms.Button btnShowHistory;
    }
}

