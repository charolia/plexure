﻿using Exercise2.CouponManagement.Models;
using Exercise2.CouponManagement.Models.Exceptions;
using Exercise2.CouponManagement.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.XPath;

namespace Exercise2.CouponManagement.Client
{
    public partial class Form1 : Form
    {
        private readonly Func<ICouponManager> couponManagerFactory;
        private List<Coupon> allCoupons;
        public Form1(Func<ICouponManager> couponManagerFactory)
        {
            InitializeComponent();
            this.couponManagerFactory = couponManagerFactory ?? throw new ArgumentNullException(nameof(couponManagerFactory));
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            var coupons = await this.couponManagerFactory().GetActiveCouponsAsync(default);

            var task = Task.Run(()=> 
            {
                this.lvActiveCoupons.Items.Clear();
                foreach (var coupon in coupons)
                {
                    var listViewItem = new ListViewItem(coupon.Id.ToString());
                    listViewItem.SubItems.Add(coupon.Title);
                    listViewItem.SubItems.Add(coupon.MaximumPerUserAllowed.ToString());
                    listViewItem.SubItems.Add(coupon.MaximunForAllUsersAllowed.ToString());
                    listViewItem.SubItems.Add(coupon.StartDate.ToShortDateString());
                    listViewItem.SubItems.Add(coupon.EndDate.ToShortDateString());
                    this.lvActiveCoupons.Items.Add(listViewItem);
                }
            });
            
            await task;
        }

        private async void btnRedeemCoupon_Click(object sender, EventArgs e)
        {
            this.btnRedeemCoupon.Enabled = false;
            this.btnCanRedeem.Enabled = false;
            this.txtCouponRedeemStatus.Text = string.Empty;

            try
            {
                var userId = int.Parse(this.txtUserId.Text);
                var couponId = long.Parse(this.txtCouponId.Text);

                try
                {
                    var result = await this.couponManagerFactory().RedeemCouponAsync(couponId, userId, default);

                    this.txtCouponRedeemStatus.Text = $"Successfully Redeemed!!\r\nID: {result.Id}\r\nCouponId: {result.CouponId}\r\nUserId: {result.UserId}\r\nRedeem Date: {result.Date}\r\nCode: {result.Code}";
                }
                catch (ArgumentException exp)
                {
                    this.txtCouponRedeemStatus.Text = $"Coupon Error: Invalid {exp.Message}";
                }
                catch (CouponRedeemException exp)
                {
                    this.txtCouponRedeemStatus.Text = $"Redemption Error: {exp.Message}";
                }
                catch (Exception exp)
                {
                    this.txtCouponRedeemStatus.Text = $"Error: {exp.Message}";
                }
            }
            catch (Exception exp)
            {
                this.txtCouponRedeemStatus.Text = $"Error occured: {exp.Message}";
            }
            finally
            {

                this.btnRedeemCoupon.Enabled = true;
                this.btnCanRedeem.Enabled = true;
            }
        }

        private async void btnCanRedeem_Click(object sender, EventArgs e)
        {
            this.btnRedeemCoupon.Enabled = false;
            this.btnCanRedeem.Enabled = false;

            this.txtCouponRedeemStatus.Text = string.Empty;

            try
            {
                var userId = int.Parse(this.txtUserId.Text);
                var couponId = long.Parse(this.txtCouponId.Text);

                var result = await this.couponManagerFactory().IsCouponRedeemableAsync(couponId, userId, default);

                if (result)
                    this.txtCouponRedeemStatus.Text = "Coupon is redeemable!";
                else
                    this.txtCouponRedeemStatus.Text = "Coupon is not redeemable :(";
            }
            catch (Exception exp)
            {
                this.txtCouponRedeemStatus.Text = $"Error occured: {exp.Message}";
            }
            finally
            {

                this.btnRedeemCoupon.Enabled = true;
                this.btnCanRedeem.Enabled = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.PopulateOffers();
        }

        private async void PopulateOffers()
        {
            this.allCoupons = await this.couponManagerFactory().GetAllCouponsAsync(default);

            var task = Task.Run(() => 
            {
                this.cbCouponOffers.Items.AddRange(this.allCoupons.Select(e => e.Title).ToArray());
            });

            await task;
        }

        private async void btnShowHistory_Click(object sender, EventArgs e)
        {
            if (this.cbCouponOffers.SelectedIndex < 0)
                return;

            this.btnShowHistory.Enabled = false;
            try
            {
                var couponId = this.allCoupons[this.cbCouponOffers.SelectedIndex].Id;

                var history = await this.couponManagerFactory().GetCouponRedemptionReportAsync(couponId, default);

                var task = Task.Run(() => 
                {
                    this.lvCouponHistory.Items.Clear();

                    this.txtTotalCouponCount.Text = history.CouponClaimedByAllTheUsers.ToString();

                    foreach (var historyItem in history.UserRedemptionHistory)
                    {
                        var listViewItem = new ListViewItem(historyItem.CouponId.ToString());
                        listViewItem.SubItems.Add(historyItem.Code.ToString());
                        listViewItem.SubItems.Add(historyItem.Date.ToShortDateString());
                        listViewItem.SubItems.Add(historyItem.UserId.ToString());
                        this.lvCouponHistory.Items.Add(listViewItem);
                    }
                });

                await task;
            }
            finally
            {
                this.btnShowHistory.Enabled = true;
            }

        }
    }
}
