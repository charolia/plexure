using Moq;
using NUnit.Framework;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Exercise2.CouponManagement.Models;
using System.Collections.Generic;

namespace Exercise3.Tests
{
    public class CouponManagerTests
    {
        private Mock<ICouponProvider> couponProviderMock;
        private Mock<ILogger> loggerMock;
        private Mock<IEnumerable<Func<Coupon, Guid, bool>>> evaluatorsMock;
        private CouponManager couponManager;


        [SetUp]
        public void Setup()
        {
            this.couponProviderMock = new Mock<ICouponProvider>();
            this.loggerMock = new Mock<ILogger>();
            this.evaluatorsMock = new Mock<IEnumerable<Func<Coupon, Guid, bool>>>();
            this.couponManager = new CouponManager(this.loggerMock.Object, this.couponProviderMock.Object);

        }

        [Test]
        public void NullParameters_ConstructorTest()
        {
            Assert.Throws<ArgumentNullException>(() => new CouponManager(null, this.couponProviderMock.Object));

            Assert.Throws<ArgumentNullException>(() => new CouponManager(this.loggerMock.Object, null));
        }

        [Test]
        public void CanRedeemCoupon_NullTest()
        {
            Assert.ThrowsAsync<ArgumentNullException>( async () => await this.couponManager.CanRedeemCoupon(It.IsAny<Guid>(), It.IsAny<Guid>(), null));

             this.couponProviderMock.Setup(x => x.Retrieve(It.IsAny<Guid>()))
                .Returns(Task.FromResult<Coupon>(null));

            Assert.ThrowsAsync<KeyNotFoundException>(async () => await this.couponManager.CanRedeemCoupon(It.IsAny<Guid>(), It.IsAny<Guid>(), this.evaluatorsMock.Object));
        }


        [Test]
        public void CanRedeem_Evaluators_Returns_True()
        {
            this.couponProviderMock.Setup(x => x.Retrieve(It.IsAny<Guid>()))
                .Returns(Task.FromResult<Coupon>(new Coupon()));

            var evaluators = new List<Func<Coupon, Guid, bool>>();

            var result = this.couponManager.CanRedeemCoupon(It.IsAny<Guid>(), It.IsAny<Guid>(), evaluators).GetAwaiter().GetResult();

            Assert.IsTrue(result);
        }

        [Test]
        public void CanRedeem_EvaluatorsCollection_Returns_False()
        {
            this.couponProviderMock.Setup(x => x.Retrieve(It.IsAny<Guid>()))
                .Returns(Task.FromResult<Coupon>(new Coupon()));

            var evaluators = new List<Func<Coupon, Guid, bool>>();
            Func<Coupon, Guid, bool> func = (coupon, id) => false;
            evaluators.Add(func);

            var result = this.couponManager.CanRedeemCoupon(It.IsAny<Guid>(), It.IsAny<Guid>(), evaluators).GetAwaiter().GetResult();

            Assert.IsFalse(result);
        }



        [Test]
        public void CanRedeem_EvaluatorsCollection_Returns_True()
        {
            this.couponProviderMock.Setup(x => x.Retrieve(It.IsAny<Guid>()))
                .Returns(Task.FromResult<Coupon>(new Coupon()));

            var evaluators = new List<Func<Coupon, Guid, bool>>();
            Func<Coupon, Guid, bool> func = (coupon, id) => true;
            evaluators.Add(func);

            var result = this.couponManager.CanRedeemCoupon(It.IsAny<Guid>(), It.IsAny<Guid>(), evaluators).GetAwaiter().GetResult();

            Assert.IsTrue(result);
        }
    }
}