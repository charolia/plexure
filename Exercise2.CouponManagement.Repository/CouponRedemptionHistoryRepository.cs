﻿using Exercise2.CouponManagement.Models;
using Exercise2.CouponManagement.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository
{
    public class CouponRedemptionHistoryRepository : ICouponRedemptionHistoryRepository
    {
        private readonly ILogger<CouponRedemptionHistoryRepository> logger;
        private readonly ICouponDataContext context;

        public CouponRedemptionHistoryRepository(ILogger<CouponRedemptionHistoryRepository> logger, ICouponDataContext context)
        {
            this.logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
            this.context = context ?? throw new System.ArgumentNullException(nameof(context));
        }

        public async Task AddAsync(CouponRedemptionHistory couponRedemptionHistory, CancellationToken cancellationToken = default)
        {
            if (couponRedemptionHistory == null)
            {
                throw new ArgumentNullException(nameof(couponRedemptionHistory));
            }

            await this.context.CouponRedemptionHistory.AddAsync(couponRedemptionHistory, cancellationToken);

            await this.context.SaveChangeAsync(true, cancellationToken);
        }

        public async Task<List<CouponRedemptionHistory>> GetAllAsync(long couponId, CancellationToken cancellationToken)
        {
            return await this.context
                             .CouponRedemptionHistory
                             .Where(e => e.CouponId == couponId)
                             .ToListAsync(cancellationToken);
        }
    }
}
