﻿using Exercise2.CouponManagement.Models;
using Exercise2.CouponManagement.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.EntityFrameworkCore.Storage;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository
{
    public class CouponDataContext : DbContext, ICouponDataContext
    {
        public CouponDataContext(DbContextOptions<CouponDataContext> options)
            : base(options) { }


        public DbSet<Coupon> Coupons { get; set; }
        public DbSet<CouponAllUserCount> CouponAllUserCounts { get; set; }
        public DbSet<CouponRedemptionHistory> CouponRedemptionHistory { get; set; }
        public DbSet<CouponUserCount> CouponUserCounts { get; set; }
        public DbSet<User> Users { get; set; }

        public Task ExecuteSqlRawAsync(string sql, IEnumerable<object> parameters, CancellationToken cancellationToken = default)
        {
            return this.Database.ExecuteSqlRawAsync(sql, parameters, cancellationToken);
        }

        public Task BeginTransactionAsync(CancellationToken cancellationToken = default)
        {
            return this.Database.BeginTransactionAsync(cancellationToken);
        }
        public void CommitTransaction()
        {
            this.Database.CommitTransaction();
        }
        public void RollbackTransaction()
        {
            this.Database.RollbackTransaction();
        }
        public Task SaveChangeAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            return this.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CouponUserCount>()
                .HasKey(c => new {c.CouponId, c.UserId});
            //modelBuilder.Entity<CouponAllUserCount>()
            //    .HasKey(c => new { c.CouponId, c.UserId });
        }
    }
}
