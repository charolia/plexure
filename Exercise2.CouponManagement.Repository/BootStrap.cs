﻿using Exercise2.CouponManagement.Repository.Interfaces;
using Exercise2.CouponManagement.Repository.Reports;
using Exercise2.CouponManagement.Repository.Reports.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Exercise2.CouponManagement.Repository
{
    public static class BootStrap
    {
        public static IServiceCollection AddRepositoryDependencies(this IServiceCollection services, Func<string> connectionStringProvider)
        {
            services.AddDbContext<CouponDataContext>(opt => opt.UseSqlServer(connectionStringProvider()));

            services
                .AddTransient<IUserRepository, UserRepository>()
                .AddTransient<ICouponRepository, CouponRepository>()
                .AddTransient<ICouponUserCountRepository, CouponUserCountRepository>()
                .AddTransient<ICouponAllUserCountRepository, CouponAllUserCountRepository>()
                .AddTransient<ICouponRedemptionHistoryRepository, CouponRedemptionHistoryRepository>()
                .AddTransient<ICouponRedemptionReportRepository, CouponRedemptionReportRepository>()

                .AddTransient<ICouponDataContext, CouponDataContext>();

            return services;

        }
    }
}
