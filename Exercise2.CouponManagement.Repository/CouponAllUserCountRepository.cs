﻿using Exercise2.CouponManagement.Models;
using Exercise2.CouponManagement.Repository.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository
{
    public class CouponAllUserCountRepository : ICouponAllUserCountRepository
    {
        private readonly ILogger<CouponAllUserCountRepository> logger;
        private readonly ICouponDataContext context;
        
        public CouponAllUserCountRepository(ILogger<CouponAllUserCountRepository> logger, ICouponDataContext context)
        {
            this.logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
            this.context = context ?? throw new System.ArgumentNullException(nameof(context));
        }

        public async Task<CouponAllUserCount> GetAsync(long couponId, CancellationToken cancellationToken)
        {
            return await this.context
                             .CouponAllUserCounts
                             .Where(e => e.CouponId == couponId)
                             .SingleOrDefaultAsync(cancellationToken);
        }

        public async Task IncrementRedeemCountAsync(long couponId, CancellationToken cancellationToken)
        {
            var sqlCmd = @"IF EXISTS (SELECT 1 FROM dbo.CouponAllUserCount WHERE [CouponId] = @CouponId)
                            BEGIN
	                            UPDATE dbo.CouponAllUserCount 
	                            SET [TotalRedeem] = [TotalRedeem] + 1
	                            WHERE [CouponId] = @CouponId
                            END
                            ELSE
                            BEGIN
	                            INSERT INTO dbo.CouponAllUserCount([CouponId], [TotalRedeem])
	                            VALUES (@CouponId, 1)
                            END";

            var parameters = new[] { new SqlParameter("@CouponId", couponId) };

            await this.context.ExecuteSqlRawAsync(sqlCmd, parameters, cancellationToken);

            await this.context.SaveChangeAsync(true, cancellationToken);
        }

        public async Task<bool> IsCouponRedeemableAsync(long couponId, int maxCouponCountForAllUsers, CancellationToken cancellationToken = default)
        {
            var couponRedeemCountInfo = await this.context.CouponAllUserCounts.SingleOrDefaultAsync(e => e.CouponId == couponId, cancellationToken);

            if (couponRedeemCountInfo == null)
                return true;
            else
                return couponRedeemCountInfo.TotalRedeem < maxCouponCountForAllUsers;
        }
    }
}
