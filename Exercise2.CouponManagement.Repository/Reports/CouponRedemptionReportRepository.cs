﻿using Exercise2.CouponManagement.Models.Reports.Models;
using Exercise2.CouponManagement.Repository.Interfaces;
using Exercise2.CouponManagement.Repository.Reports.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository.Reports
{
    public class CouponRedemptionReportRepository : ICouponRedemptionReportRepository
    {
        private readonly ILogger<CouponRedemptionReportRepository> logger;
        private readonly ICouponRepository couponRepository;
        private readonly ICouponAllUserCountRepository couponAllUserCountRepository;
        private readonly ICouponRedemptionHistoryRepository couponRedemptionHistoryRepository;

        public CouponRedemptionReportRepository(
            ILogger<CouponRedemptionReportRepository> logger, 
            ICouponRepository couponRepository,
            ICouponAllUserCountRepository couponAllUserCountRepository,
            ICouponRedemptionHistoryRepository couponRedemptionHistoryRepository)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.couponRepository = couponRepository ?? throw new ArgumentNullException(nameof(couponRepository));
            this.couponAllUserCountRepository = couponAllUserCountRepository ?? throw new ArgumentNullException(nameof(couponAllUserCountRepository));
            this.couponRedemptionHistoryRepository = couponRedemptionHistoryRepository ?? throw new ArgumentNullException(nameof(couponRedemptionHistoryRepository));
        }

        public async Task<CouponRedemptionReportModel> GetCouponHistoryByOfferAsync(long couponId, CancellationToken cancellationToken = default)
        {
            return new CouponRedemptionReportModel
            {
                Coupon = await this.couponRepository.GetAsync(couponId, cancellationToken),
                CouponClaimedByAllTheUsers = (await this.couponAllUserCountRepository.GetAsync(couponId, cancellationToken))?.TotalRedeem ?? 0,
                UserRedemptionHistory = await this.couponRedemptionHistoryRepository.GetAllAsync(couponId, cancellationToken)
            };
        }
    }
}
