﻿using Exercise2.CouponManagement.Models.Reports.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository.Reports.Interfaces
{
    public interface ICouponRedemptionReportRepository
    {
        Task<CouponRedemptionReportModel> GetCouponHistoryByOfferAsync(long couponId, CancellationToken cancellationToken);
    }
}
