﻿using Exercise2.CouponManagement.Models;
using Exercise2.CouponManagement.Models.Exceptions;
using Exercise2.CouponManagement.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository
{
    public class CouponRepository : ICouponRepository
    {
        private readonly ILogger<CouponRepository> logger;
        private readonly ICouponDataContext context;
        private readonly ICouponAllUserCountRepository couponAllUserCountRepository;
        private readonly ICouponUserCountRepository couponUserCountRepository;
        private readonly ICouponRedemptionHistoryRepository couponRedemptionHistoryRepository;
        private readonly IUserRepository userRepository;

        public CouponRepository(
            ILogger<CouponRepository> logger, 
            ICouponDataContext context,
            ICouponAllUserCountRepository couponAllUserCountRepository,
            ICouponUserCountRepository couponUserCountRepository,
            ICouponRedemptionHistoryRepository couponRedemptionHistoryRepository,
            IUserRepository userRepository)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.couponAllUserCountRepository = couponAllUserCountRepository ?? throw new ArgumentNullException(nameof(couponAllUserCountRepository));
            this.couponUserCountRepository = couponUserCountRepository ?? throw new ArgumentNullException(nameof(couponUserCountRepository));
            this.couponRedemptionHistoryRepository = couponRedemptionHistoryRepository ?? throw new ArgumentNullException(nameof(couponRedemptionHistoryRepository));
            this.userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task<List<Coupon>> GetActiveCouponsAsync(CancellationToken cancellationToken = default)
        {
            var current = DateTime.Now;
            return await this.context.Coupons
                .Where(e => e.StartDate <= current && current <= e.EndDate)
                .ToListAsync(cancellationToken);
        }

        public async Task<List<Coupon>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            return await this.context.Coupons.ToListAsync(cancellationToken);
        }

        public async Task<Coupon> GetAsync(long id, CancellationToken cancellationToken = default)
        {
            if (id < 0)
                throw new ArgumentException(nameof(id));
            
            var current = DateTime.Now;

            return await this.context.Coupons.FirstOrDefaultAsync(e => e.Id == id && e.StartDate <= current && current <= e.EndDate, cancellationToken);
        }

        public async Task<Coupon> GetCouponByOfferAsync(string title, CancellationToken cancellationToken = default)
        {
            if (string.IsNullOrWhiteSpace(title))
                return null;

            return await this.context.Coupons.FirstOrDefaultAsync(e => e.Title == title, cancellationToken);
        }

        public async Task<bool> IsCouponRedeemableAsync(long id, int userId, CancellationToken cancellationToken = default)
        {
            var coupon = await this.GetAsync(id, cancellationToken);
            var user = await this.userRepository.GetAsync(userId, cancellationToken);

            if (coupon == null || user == null)
            {
                return false;
            }

            if (!await this.couponUserCountRepository.IsCouponRedeemableAsync(id, userId, coupon.MaximumPerUserAllowed, cancellationToken))
            {
                return false;   
            }

            if (!await this.couponAllUserCountRepository.IsCouponRedeemableAsync(id, coupon.MaximunForAllUsersAllowed, cancellationToken))
            {
                return false;
            }

            return true;
        }

        public async Task<CouponRedemptionHistory> RedeemCouponAsync(long id, int userId, CancellationToken cancellationToken = default)
        {
            var coupon = await this.GetAsync(id, cancellationToken);

            if (coupon == null)
            {
                throw new ArgumentException(nameof(id));
            }

            var user = await this.userRepository.GetAsync(userId, cancellationToken);
            if (user == null)
            {
                throw new ArgumentException(nameof(userId));
            }

            if (!await this.couponUserCountRepository.IsCouponRedeemableAsync(id, userId, coupon.MaximumPerUserAllowed, cancellationToken))
            {
                throw new CouponRedeemException("Coupon maximum limit reached per user.");
            }

            if (!await this.couponAllUserCountRepository.IsCouponRedeemableAsync(id, coupon.MaximunForAllUsersAllowed, cancellationToken))
            {
                throw new CouponRedeemException("Coupon maximum limit reached for all users.");
            }

            try
            {
                var couponHistory = new CouponRedemptionHistory
                {
                    Code = Guid.NewGuid(),
                    CouponId = id,
                    UserId = userId,
                    Date = DateTime.Now
                };

                await this.context.BeginTransactionAsync(cancellationToken);

                await this.couponAllUserCountRepository.IncrementRedeemCountAsync(id, cancellationToken);
                await this.couponUserCountRepository.IncrementRedeemCountAsync(id, userId, cancellationToken);

                await this.couponRedemptionHistoryRepository.AddAsync(couponHistory, cancellationToken);

                this.context.CommitTransaction();
                
                return couponHistory;
            }
            catch (Exception exception)
            {
                this.context.RollbackTransaction();

                throw new CouponRedeemException("Failed to redeem coupon.", exception);
            }
        }
    }
}
