﻿using Exercise2.CouponManagement.Models;
using Exercise2.CouponManagement.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly ILogger<UserRepository> logger;
        private readonly ICouponDataContext context;

        public UserRepository(ILogger<UserRepository> logger, ICouponDataContext context)
        {
            this.logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
            this.context = context ?? throw new System.ArgumentNullException(nameof(context));
        }

        public async Task<User> GetAsync(int id, CancellationToken cancellationToken = default)
        {
            if (id < 0)
                throw new ArgumentException(nameof(id));

            return await context.Users.FirstOrDefaultAsync(e => e.Id == id, cancellationToken);
        }
    }
}
