﻿using Exercise2.CouponManagement.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository.Interfaces
{
    public interface ICouponRedemptionHistoryRepository
    {
        Task<List<CouponRedemptionHistory>> GetAllAsync(long couponId, CancellationToken cancellationToken);

        Task AddAsync(CouponRedemptionHistory couponRedemptionHistory, CancellationToken cancellationToken);
    }
}
