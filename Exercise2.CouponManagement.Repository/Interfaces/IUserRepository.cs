﻿using Exercise2.CouponManagement.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository.Interfaces
{
    public interface IUserRepository
    {
        Task<User> GetAsync(int id, CancellationToken cancellationToken);
    }
}
