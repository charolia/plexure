﻿using Exercise2.CouponManagement.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository.Interfaces
{
    public interface ICouponDataContext
    {
        public DbSet<Coupon> Coupons { get; set; }
        public DbSet<CouponAllUserCount> CouponAllUserCounts { get; set; }
        public DbSet<CouponRedemptionHistory> CouponRedemptionHistory { get; set; }
        public DbSet<CouponUserCount> CouponUserCounts { get; set; }
        public DbSet<User> Users { get; set; }

        void RollbackTransaction();
        
        Task BeginTransactionAsync(CancellationToken cancellationToken);
        
        void CommitTransaction();

        Task SaveChangeAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken);

        Task ExecuteSqlRawAsync(string sql, IEnumerable<object> parameters, CancellationToken cancellationToken);
    }
}
