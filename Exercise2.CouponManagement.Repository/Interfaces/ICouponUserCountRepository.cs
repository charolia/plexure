﻿
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository.Interfaces
{
    public interface ICouponUserCountRepository
    {
        Task<bool> IsCouponRedeemableAsync(long couponId, int userId, int maxCouponCountForUser, CancellationToken cancellationToken);

        Task IncrementRedeemCountAsync(long couponId, int userId, CancellationToken cancellationToken);
    }
}
