﻿
using Exercise2.CouponManagement.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository.Interfaces
{
    public interface ICouponAllUserCountRepository
    {
        Task<CouponAllUserCount> GetAsync(long couponId, CancellationToken cancellationToken);

        Task<bool> IsCouponRedeemableAsync(long couponId, int maxCouponCountForAllUsers, CancellationToken cancellationToken);

        Task IncrementRedeemCountAsync(long couponId,  CancellationToken cancellationToken);
    }
}
