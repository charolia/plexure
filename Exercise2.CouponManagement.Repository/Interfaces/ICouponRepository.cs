﻿using Exercise2.CouponManagement.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository.Interfaces
{
    public interface ICouponRepository
    {
        Task<List<Coupon>> GetAllAsync(CancellationToken cancellationToken);

        Task<Coupon> GetAsync(long id, CancellationToken cancellationToken);

        Task<List<Coupon>> GetActiveCouponsAsync(CancellationToken cancellationToken);

        Task<Coupon> GetCouponByOfferAsync(string title, CancellationToken cancellationToken);

        Task<CouponRedemptionHistory> RedeemCouponAsync(long id, int userId, CancellationToken cancellationToken);

        Task<bool> IsCouponRedeemableAsync(long id, int userId, CancellationToken cancellationToken);
    }
}
