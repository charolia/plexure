﻿using Exercise2.CouponManagement.Models;
using Exercise2.CouponManagement.Repository.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.ComponentModel.DataAnnotations;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Repository
{
    public class CouponUserCountRepository : ICouponUserCountRepository
    { 
        private readonly ILogger<CouponUserCountRepository> logger;
        private readonly ICouponDataContext context;
        
        public CouponUserCountRepository(ILogger<CouponUserCountRepository> logger, ICouponDataContext context)
        {
            this.logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
            this.context = context ?? throw new System.ArgumentNullException(nameof(context));
        }

        public async Task IncrementRedeemCountAsync(long couponId, int userId, CancellationToken cancellationToken = default)
        {
            var sqlCmd = @"IF EXISTS (SELECT 1 FROM dbo.CouponUserCount WHERE [CouponId] = @CouponId AND [UserId] = @UserId)
                            BEGIN
	                            UPDATE dbo.CouponUserCount 
	                            SET [TotalRedeem] = [TotalRedeem] + 1
	                            WHERE [CouponId] = @CouponId
	                            AND [UserId] =@UserId
                            END
                            ELSE
                            BEGIN
	                            INSERT INTO dbo.CouponUserCount([UserId],[CouponId], [TotalRedeem])
	                            VALUES (@UserId, @CouponId, 1)
                            END";

            var parameters = new[] { new SqlParameter("@UserId", userId), new SqlParameter("@CouponId", couponId) };

            await this.context.ExecuteSqlRawAsync(sqlCmd, parameters, cancellationToken);

            await this.context.SaveChangeAsync(true, cancellationToken);
        }

        public async Task<bool> IsCouponRedeemableAsync(long couponId, int userId, int maxCouponCountForUser, CancellationToken cancellationToken = default)
        {
            var couponUserCount = await this.context.CouponUserCounts.SingleOrDefaultAsync(e => e.CouponId == couponId && e.UserId == userId, cancellationToken);

            if (couponUserCount == null)
                return true;
            else
                return couponUserCount.TotalRedeem < maxCouponCountForUser;
        }
    }
}
