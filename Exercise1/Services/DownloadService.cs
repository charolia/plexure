﻿using Exercise1.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise1.Services
{
    public class DownloadService : IDownloadService
    {
        private readonly HttpClient httpClient;

        public DownloadService()
        {
            httpClient = new HttpClient();
        }


        public async Task<string> GetContentAsync(string url, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(url))
                return string.Empty;

            if (cancellationToken == null)
                throw new ArgumentNullException(nameof(cancellationToken));

            try
            {
                var response = await httpClient.GetAsync(url, cancellationToken);

                if (cancellationToken.IsCancellationRequested)
                    return string.Empty;

                return await response.Content.ReadAsStringAsync();
            }
            catch (OperationCanceledException)
            {
                /// If user chooses to chancel the request in this case 
                /// we  do not want to propgage the exception to the calling function.
                if (!cancellationToken.IsCancellationRequested)
                    throw;
            }

            return string.Empty;

        }
    }
}
