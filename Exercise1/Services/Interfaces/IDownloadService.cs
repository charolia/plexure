﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise1.Services.Interfaces
{
    /// <summary>
    /// This interface defines contracts to download content over HTTP.
    /// </summary>
    public interface IDownloadService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        Task<string> GetContentAsync (string url, CancellationToken cancellationToken);
    }
}
