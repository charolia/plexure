﻿using Exercise1.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise1
{
    public partial class Exercise1 : Form
    {
        private const string GoogleUrl = "https://www.google.com";
        private const string BingUrl = "https://www.bing.com";
        private const string YahooUrl = "https://www.yahoo.com";

        private readonly string[] webPages = new []{ GoogleUrl, BingUrl, YahooUrl };

        private readonly IDownloadService downloadService;

        private CancellationTokenSource cancellationTokenSource;

        public Exercise1(IDownloadService downloadService)
        {
            InitializeComponent();
            this.downloadService = downloadService ?? throw new ArgumentNullException(nameof(downloadService));
        }

        private async void BtnGetResult_Click(object sender, EventArgs e)
        {
            this.btnCancel.Enabled = true;
            this.btnGetResult.Enabled = false;
            this.txtResult.Text = string.Empty;

            this.cancellationTokenSource = new CancellationTokenSource();

            var contentLength = 0;

            var tasks = this.webPages.Select(async page =>
                {
                    var length = (await this.downloadService.GetContentAsync(page, this.cancellationTokenSource.Token)).Length;
                    Interlocked.Add(ref contentLength, length);
                });

            await Task.WhenAll(tasks);

            if (this.cancellationTokenSource.IsCancellationRequested)
                this.txtResult.Text = $"User cancelled the operation.";
            else
                this.txtResult.Text = $"Content length from {this.webPages.Length} ({string.Join(", ", this.webPages)}) services is : {contentLength}";

            this.cancellationTokenSource = null;

            this.btnGetResult.Enabled = true;
            this.btnCancel.Enabled = false;
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.cancellationTokenSource == null)
                return;

            this.cancellationTokenSource.Cancel();

            this.btnGetResult.Enabled = true;
            this.btnCancel.Enabled = false;
        }
    }
}
