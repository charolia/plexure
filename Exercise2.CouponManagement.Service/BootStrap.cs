﻿using Exercise2.CouponManagement.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Exercise2.CouponManagement.Repository;
using System;

namespace Exercise2.CouponManagement.Services
{
    public class BootStrap
    {
        public static IServiceCollection SetupDependencies(IServiceCollection services, Func<string> connectionStringProvider)
        {
            services
                .AddRepositoryDependencies(connectionStringProvider)
                .AddTransient<ICouponManager, CouponManager>();

            return services;
        }
    }
}
