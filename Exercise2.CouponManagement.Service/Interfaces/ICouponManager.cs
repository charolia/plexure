﻿using Exercise2.CouponManagement.Models;
using Exercise2.CouponManagement.Models.Reports.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Services.Interfaces
{
    public interface ICouponManager
    {
        Task<List<Coupon>> GetAllCouponsAsync(CancellationToken  cancellationToken);

        Task<List<Coupon>> GetActiveCouponsAsync(CancellationToken cancellationToken);

        Task<bool> IsCouponRedeemableAsync(long couponId, int userId, CancellationToken cancellationToken);

        Task<CouponRedemptionHistory> RedeemCouponAsync(long couponId, int userId, CancellationToken cancellationToken);

        Task<CouponRedemptionReportModel> GetCouponRedemptionReportAsync(long couponId, CancellationToken cancellationToken);
    }
}
