﻿using Exercise2.CouponManagement.Models;
using Exercise2.CouponManagement.Models.Reports.Models;
using Exercise2.CouponManagement.Repository.Interfaces;
using Exercise2.CouponManagement.Repository.Reports.Interfaces;
using Exercise2.CouponManagement.Services.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Exercise2.CouponManagement.Services
{
    public class CouponManager : ICouponManager
    {
        private readonly ILogger<CouponManager> logger;
        private readonly ICouponRepository couponRepository;
        private readonly ICouponRedemptionReportRepository couponRedemptionReportRepository;

        public CouponManager(
            ILogger<CouponManager> logger, 
            ICouponRepository couponRepository,
            ICouponRedemptionReportRepository couponRedemptionReportRepository)
        {
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
            this.couponRepository = couponRepository ?? throw new ArgumentNullException(nameof(couponRepository));
            this.couponRedemptionReportRepository = couponRedemptionReportRepository ?? throw new ArgumentNullException(nameof(couponRedemptionReportRepository));
        }

        public async Task<List<Coupon>> GetActiveCouponsAsync(CancellationToken cancellationToken = default)
        {
            return await this.couponRepository.GetActiveCouponsAsync(cancellationToken);
        }

        public async Task<List<Coupon>> GetAllCouponsAsync(CancellationToken cancellationToken)
        {
            return await this.couponRepository.GetAllAsync(cancellationToken);
        }

        public async Task<CouponRedemptionReportModel> GetCouponRedemptionReportAsync(long couponId, CancellationToken cancellationToken = default)
        {
            return await this.couponRedemptionReportRepository.GetCouponHistoryByOfferAsync(couponId, cancellationToken);
        }

        public async Task<bool> IsCouponRedeemableAsync(long couponId, int userId, CancellationToken cancellationToken = default)
        {
            return await this.couponRepository.IsCouponRedeemableAsync(couponId, userId, cancellationToken);
        }

        public async Task<CouponRedemptionHistory> RedeemCouponAsync(long couponId, int userId, CancellationToken cancellationToken = default)
        {
            return await this.couponRepository.RedeemCouponAsync(couponId, userId, cancellationToken);
        }
    }
}
